﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeworkStyles
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int count = 0;
        public MainWindow()
        {
            InitializeComponent();
            this.Resources = new ResourceDictionary() { Source = new Uri(@"C:\Users\User\source\repos\homework_styles\HomeworkStyles\OriginalTheme.xaml") };

        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            SolidColorBrush mySolidColorBrushForRectangle = new SolidColorBrush();
            count++;
            if (count % 2 == 1)
            {
                this.Resources = new ResourceDictionary() { Source = new Uri(@"C:\Users\User\source\repos\homework_styles\HomeworkStyles\DarkTheme.xaml") };
                mySolidColorBrush.Color = Color.FromRgb(100, 99, 99);
                bottomRectangle.Fill = mySolidColorBrush;
                listBoxItemSearch.Background = mySolidColorBrush;
            }
            else
            {
                this.Resources = new ResourceDictionary() { Source = new Uri(@"C:\Users\User\source\repos\homework_styles\HomeworkStyles\OriginalTheme.xaml") };
                mySolidColorBrushForRectangle.Color = Colors.White;
                bottomRectangle.Fill = mySolidColorBrushForRectangle;
                mySolidColorBrush.Color = Colors.LightGray;
                listBoxItemSearch.Background = mySolidColorBrush;
            }
        }

    }
}
